import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Deck from './Deck';
import Player from './Player';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode( div );
});

describe( 'App', () => {
  
  it( 'should render the Deck', () => {
    const wrapper = shallow( <App /> );
    expect( wrapper.containsMatchingElement( <Deck /> ) ).toEqual( true );
  } );

  it( 'should render a two buttons Add Player and Find Winner', () => {
    const wrapper = shallow( <App /> );
    expect(wrapper.find('Button')).toHaveLength(2);
  } );

  it( 'should render correctly', () => { const wrapper = shallow( <App /> ); expect( wrapper ).toMatchSnapshot() } );
  
  it( 'should add a new Player ', () => {
    const wrapper = shallow( <App /> );
    wrapper.setState({
      players: []
    } );

    const event = {
      preventDefault: () => true
    };
  
    wrapper
      .find('Button')
      .first()
      .simulate('click', event);
      expect(wrapper.find('Player')).toHaveLength(1);
  } );

} );